﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DoKnow.CloudEdu.CMS.Host.Models;

namespace DoKnow.CloudEdu.CMS.Host.Controllers
{
    public class SharedController : BaseController
    {
        //
        // GET: /Shared/

        public ActionResult Index()
        {
            return View();
        }
        [ChildActionOnly]
        public ActionResult _TopNavMenu()
        {
            //ViewBag.PageSize = PageSize;
            return PartialView();
        }
        [ChildActionOnly]
        public ActionResult _LeftNavMenu()
        {
            if (null != CurrentUser.UserRoles && CurrentUser.UserRoles.Count > 0)
            {
                var roleIds = CurrentUser.UserRoles.Select(i => i.RoleID).Distinct();
                #if DEBUG
                var modules = CurCache.BSModuleFull;
                #else
                var moduleIds = CurCache.BSRoleModuleFull.Where(o => roleIds.Contains(o.RoleID)).Select(o => o.ModuleID).Distinct();
                var modules = CurCache.BSModuleFull.Where(o => moduleIds.Contains(o.ID)).ToList();
                var parentModuleIds = modules.Select(o => o.ParentID).Distinct();
                modules = CurCache.BSModuleFull.Where(i => parentModuleIds.Contains(i.ID)).Concat(modules).ToList();
                #endif
                var pagePath = HttpContext.Items[Request_Path_Item_Key] as string;
                var menuItemPath = HttpContext.Items[Menu_Path_Item_Key] as string;
                var model = new List<MenuItemModel>();
                foreach (var item in modules.Where(o => o.ParentID == 0).OrderBy(o => o.ModuleOrder))
                {
                    var node = new MenuItemModel()
                    {
                        Children = modules.Where(o => o.ParentID == item.ID).OrderBy(o => o.ModuleOrder).Select(o =>
                            new MenuItemModel
                            {
                                Children = null,
                                IconClass = null,
                                ID = o.ID,
                                IsActive = o.ModuleUrl.ToLower().IndexOf(pagePath) == 0,
                                Text = o.ModuleName,
                                Url = o.ModuleUrl
                            }).ToList(),
                        IconClass = item.ModuleIcon,
                        ID = item.ID,
                        IsActive = item.ModuleUrl.ToLower().IndexOf(pagePath) == 0,
                        Text = item.ModuleName,
                        Url = item.ModuleUrl
                    };

                    if (node.Children.Exists(o => o.IsActive))
                    {
                        node.IsActive = true;
                    }
                    else if (string.IsNullOrEmpty(menuItemPath) == false)
                    {
                        var activeItem = node.Children.FirstOrDefault(o => o.Url.ToLower().IndexOf(menuItemPath) == 0);
                        if (activeItem != null)
                        {
                            activeItem.IsActive = true;
                            node.IsActive = true;
                        }
                    }
                    model.Add(node);
                }
                return PartialView(model);
            }

            return PartialView();
            //return PartialView(menuList);
        }
    }
}
