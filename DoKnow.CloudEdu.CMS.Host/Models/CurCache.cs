﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using DoKnow.CloudEdu.Common.Consts;
using DoKnow.CloudEdu.DataContract.Model.Base;
using DoKnow.Common.Utility;
using DoKnow.Common.Wrapper;
using Newtonsoft.Json;

namespace DoKnow.CloudEdu.CMS.Host.Models
{
    public class CurCache
    {

        public static string ServiceHost
        {
            get
            {
                if(string.IsNullOrEmpty(ConfigurationManager.AppSettings["ServiceHost"]))
                {
                    return "http://localhost:52261/";
                }
                return ConfigurationManager.AppSettings["ServiceHost"];
                
            }
        }
        public static ModuleDatas BSModuleFull
        {
            get
            {
                if (null == CacheHelper.Get<ModuleDatas>(CacheKey.BSModuleFull))
                    LoadModuleFull();
                return CacheHelper.Get<ModuleDatas>(CacheKey.BSModuleFull);
            }
        }

        private static void LoadModuleFull()
        {
            try
            {
                ModuleDatas modules = HttpHelper.Post<ModuleDatas>(ServiceHost + ServicePath.ModuleRead,
                    JsonConvert.SerializeObject(new { ActionName = "RetrieveBSModules", Pars = new { where = "" } }));
                if (null != modules && modules.Count > 0)
                {
                    CacheHelper.Insert<ModuleDatas>(CacheKey.BSModuleFull, modules);
                }
            }
            catch (Exception ex)
            {
                //Log4NetLogger.Trace().Error(string.Format("Excetion Message: {0}", ex.Message));
            }
        }

        public static List<BSRoleModuleInfo> BSRoleModuleFull
        {
            get
            {
                if (null == CacheHelper.Get<RoleModuleDatas>(CacheKey.BSRoleModuleFull))
                    LoadRoleModuleFull();
                return CacheHelper.Get<List<BSRoleModuleInfo>>(CacheKey.BSRoleModuleFull);
            }
        }

        private static void LoadRoleModuleFull()
        {
            try
            {
                RoleModuleDatas roleModules = HttpHelper.Post<RoleModuleDatas>(ServiceHost + ServicePath.RoleRead,
                    JsonConvert.SerializeObject(new { ActionName = "RetrieveBSRoleModules", Pars = new { where = "" } }));
                if (null != roleModules && roleModules.RoleModules.Count > 0)
                {
                    CacheHelper.Insert<List<BSRoleModuleInfo>>(CacheKey.BSRoleModuleFull, roleModules.RoleModules);
                }
            }
            catch (Exception ex)
            {
                //Log4NetLogger.Trace().Error(string.Format("Excetion Message: {0}", ex.Message));
            }
        }
    }
}