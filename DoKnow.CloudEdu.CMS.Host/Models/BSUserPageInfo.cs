﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using DoKnow.CloudEdu.DataContract.Model.Base;

namespace DoKnow.CloudEdu.CMS.Host.Models
{
    public class BSUserPageInfo:BSUserInfo
    {
        //public bool IsDelete { get; set; }
        public int RoleID { get; set; }
        public bool NotUnder { get; set; }

        private int _PageIndex = 1;
        public int PageIndex
        {
            get { return _PageIndex;}

            set {_PageIndex = value;}
        }
        private int _PageSize = 10;
        public int PageSize { 
            get { return _PageSize;}
            set { _PageIndex = value;}
        }
    }
}