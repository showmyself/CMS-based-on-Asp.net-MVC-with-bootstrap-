﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DoKnow.CloudEdu.Common.Consts;
using DoKnow.CloudEdu.DataContract.Model.Base;

namespace DoKnow.CloudEdu.CMS.Host.Models
{
    [Serializable]
    public class CurSession
    {
        public static UserData User
        {
            get
            {
                HttpCookie authCookie = HttpContext.Current.Request.Cookies[CookiesKey.LoginToken];
                if (authCookie == null || string.IsNullOrEmpty(authCookie.Value))
                {
                    return null;
                }
                return HttpContext.Current.Session[authCookie.Value] as UserData;
            }
        }
    }
}