﻿//获取url参数
function getQueryStringByName(name) {
    var result = location.search.match(new RegExp("[\?\&]" + name + "=([^\&]+)", "i"));
    if (result == null || result.length < 1) {
        return "";
    }
    return result[1];

}

//日期格式化
function formatCSTDate(strDate, format) {
    if (strDate && strDate.indexOf('T', 0) > -1) {
        var isChrome = (navigator.userAgent.toLowerCase().match(/chrome/) != null);
        //假如是chrome浏览器，就得减掉8个小时因为在转中国时区日期格式的时候会默认加8个小时
        if (isChrome) {
            var getDate = new Date(strDate);
            var newDate = getDate.setHours(getDate.getHours() - 8);
            return formatDate(new Date(newDate), format);
        }
    }
    return formatDate(new Date(strDate), format);
}

//日期格式化
function formatDate(date, format) {
    var paddNum = function (num) {
        num += "";
        return num.replace(/^(\d)$/, "0$1");
    }
    //指定格式字符
    var cfg = {
        yyyy: date.getFullYear() //年 : 4位
      , yy: date.getFullYear().toString().substring(2)//年 : 2位
      , M: date.getMonth() + 1  //月 : 如果1位的时候不补0
      , MM: paddNum(date.getMonth() + 1) //月 : 如果1位的时候补0
      , d: date.getDate()   //日 : 如果1位的时候不补0
      , dd: paddNum(date.getDate())//日 : 如果1位的时候补0
      , hh: date.getHours()  //时
      , mm: date.getMinutes() //分
      , ss: date.getSeconds() //秒
    }
    format || (format = "yyyy-MM-dd hh:mm:ss");
    return format.replace(/([a-z])(\1)*/ig, function (m) { return cfg[m]; });
}

//金额大写转换函数
function DX(n) {
    var unit = "千百拾亿千百拾万千百拾元角分", str = "";
    n += "00";
    var p = n.indexOf('.');
    if (p >= 0)
        n = n.substring(0, p) + n.substr(p + 1, 2);
    unit = unit.substr(unit.length - n.length);
    for (var i = 0; i < n.length; i++)
        str += '零壹贰叁肆伍陆柒捌玖'.charAt(n.charAt(i)) + unit.charAt(i);
    return str.replace(/零(千|百|拾|角)/g, "零").replace(/(零)+/g, "零").replace(/零(万|亿|元)/g, "$1").replace(/(亿)万|壹(拾)/g, "$1$2").replace(/^元零?|零分/g, "").replace(/元$/g, "元整");
}

//阻止事件(包括冒泡和默认行为)
function stopBubble(e) {
    e = e || window.event;
    if (e.preventDefault) {
        e.preventDefault();
        e.stopPropagation();
    } else {
        e.returnValue = false;
        e.cancelBubble = true;
    }
}

//为防止微信缓存页面增加时间戳
function gopage(url) {
    var timestamp = Date.parse(new Date());
    window.location.href = url + "?ts=" + timestamp;
}

//返回主题状态中文名称
function GetTopicStatusName(value) {
    var name = "";
    switch (value + "") {
        case "0":
            name = "录入中";
            break;
        case "1":
            name = "取消";
            break;
        case "2":
            name = "推广中";
            break;
        case "3":
            name = "下市";
            break;
        default:
            break;
    }
    return name;
}

//返回主题状态值
function GetTopicStatusValue(name) {
    var value = "";
    switch (name) {
        case "录入中":
            value = "0";
            break;
        case "取消":
            value = "1";
            break;
        case "上市":
            value = "2";
            break;
        case "下市":
            value = "3";
            break;
        default:
            break;
    }
    return value;
}

//返回订单类型中文名称
function GetOrderTypeName(value) {
    var name = "";
    switch (value + "") {
        case "0":
            name = "零售";
            break;
        case "1":
            name = "团购";
            break;
        default:
            break;
    }
    return name;
}

//返回订单来源中文名称
function GetOrderSourceName(value) {
    var name = "";
    switch (value + "") {
        case "0":
            name = "微信";
            break;
        case "1":
            name = "易迅";
            break;
        default:
            name = value
            break;
    }
    return name;
}

//返回订单状态中文名称
function GetOrderStatusName(value) {
    var name = "";
    switch (value + "") {
        case "0":
            name = "已保存";
            break;
        case "1"://已确认
        case "2"://已审核
        case "3"://已分配
        case "4"://拣货中
            name = "已付款";
            break;
        case "5":
            name = "已发货";
            break;
        case "6":
            name = "已接收";
            break;
        case "7":
            name = "取消申请";
            break;
        case "8":
            name = "已取消";
            break;
        case "9":
            name = "已关闭";
            break;
        case "10":
            name = "已退款";
            break;
        case "11":
            name = "退货申请中";
            break;
        case "12":
            name = "退货中";
            break;
        case "13":
            name = "退货完成";
            break;
        case "14":
            name = "退款申请中";
            break;
        default:
            break;
    }
    return name;
}

//返回订单付款方式中文名称
function GetOrderPaymentName(value) {
    var name = "";
    switch (value + "") {
        case "0":
            name = "货到付款";
            break;
        case "1":
            name = "款到发货";
            break;
        default:
            break;
    }
    return name;
}
//返回取消订单状态中文名称
function GetOrderChangeName(value) {
    var name = "";
    switch (value + "") {
        case "0":
            name = "申请中";
            break;
        case "1":
            name = "已审核";
            break;
        case "2":
            name = "已驳回";
            break;
        case "3":
            name = "已取消";
            break;
        case "9":
            name = "已完成";
            break;
        default:
            break;
    }
    return name;
}
window.MBFunction = (function () {
    return {
        StrSubString: function (o, len) {
            var str = o;
            if (o.length > 0) {
                str = o.toString().substr(0, len) + "....";
            }
            return str;
        }
    }
}());

//增加正则表达式,嵌入CDATA段可以防止不兼容Javacript的浏览器不产生错误信息
String.prototype.getQueryString = function (name) {
    var reg = new RegExp("(^|&|\\?)" + name + "=([^&]*)(&|$)"), r;
    if (r = this.match(reg)) return unescape(r[2]);
    return null;
};

//针对两种浏览器，分别获取xmlDocument对象// 读取XML文件   
function loadXML(xmlFile) {
    var xmlDoc;
    if (window.ActiveXObject) {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = false;
        xmlDoc.load(xmlFile);
    }
    else if (document.implementation && document.implementation.createDocument) {
        //Firefox，Chrome 浏览器中读取xml文件
        var xmlhttp = new window.XMLHttpRequest();
        xmlhttp.open("GET", xmlFile, false);
        xmlhttp.send(null);
        xmlDom = xmlhttp.responseXML;
    }
    else {
        xmlDom = null;
    }
    return xmlDom;
}

//读取xml中的节点
function ReadXml(code, brandType) {
    var xmlDoc = loadXML("../Configs/Config.xml");
    var vips = xmlDoc.getElementsByTagName("VipLevel");
    var maxRes = vips.length;
    var url;
    for (var i = 0; i < maxRes; i++) {
        var brand = vips[i].attributes[0].value;
        var levelcode = vips[i].attributes[1].value;
        if (levelcode == code && brandType == brand) {
            url = vips[i].attributes[2].value;
            return url;
        }
    }
    return url;
}