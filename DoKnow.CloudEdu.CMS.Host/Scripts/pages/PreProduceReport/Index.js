﻿$(document).ready(function () {
    GetReportData();
});

function GetReportData() {
    var querydata = GetQueryData();
    $.ajax({
        type: "POST",
        url: "/PreProduceReport/GetReportData",
        data: { objReport: querydata },

        success: function (data) {

            $('#canvasDiv').highcharts({
                chart: {
                    type: data.chartConfig.Type
                },
                title: {
                    text: data.chartConfig.Title
                },
                subtitle: {
                    text: data.chartConfig.Subtitle
                },
                xAxis: {
                    categories: eval(data.chartConfig.XAxis.CategoriesJson)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: data.chartConfig.YAxis.Title
                    }
                },
                tooltip: {
                    shared: true,
                    useHTML: true
                },

                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    },
                    series: {
                        cursor: 'pointer',
                        events: {
                            click: function (event) {
                                var s = event.point;
                                //alert(event.point.series.name)
                                //alert(event.point.category)
                                ReportClick(event.point.series.name, event.point.category);
                            }
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                series: eval(data.chartConfig.SeriesJson)
            });
        }
    });
}

function GetQueryData() {


    var QueryData = "{";
    if ($("#HOPE_RECEVIE_TIME_START").val() != "")
    {
        //订单时间
        QueryData += '"HOPE_RECEVIE_TIME_START":"' + $("#HOPE_RECEVIE_TIME_START").val() + '",';
    }
   
    if ($("#HOPE_RECEVIE_TIME_END").val() != "") {
        //结束时间
        QueryData += '"HOPE_RECEVIE_TIME_END":"' + $("#HOPE_RECEVIE_TIME_END").val() + '",';
    }
    

    //备注
    QueryData += '"STATUS":"' + $("#STATUS").val() + '"';



    QueryData += "}";

    return QueryData;
}

function renderTime(data) {
    var da = eval('new ' + data.replace('/', '', 'g').replace('/', '', 'g'));
    return da.getFullYear() + "-" + da.getMonth() + "-" + da.getDay() + "-" + da.getHours() + ":" + da.getSeconds() + ":" + da.getMinutes();
}

function ReportClick(status,date) {
    $("#produceinfo").html("");
    $.ajax({
        type: "POST",
        url: "/PreProduceReport/GetProduceData?status=" + status + "&date=" + date, 
        success: function (data) {
            $("#produceinfo").html("");
            var dataArray = eval(data.ProduceData);
            
            if (data.ProduceData == null) {
                 
                return;
            }
            var tableStr = "<table class=\"table table-striped table-hover table-bordered\" >";
            tableStr = tableStr + "<thead><td>订单编号</td><td>客户名称</td><td>商品编码</td><td>商品名称</td><td>定制数量</td><td>期望收货时间</td><td>生产状态</td></thead>";
            var len = dataArray.length;

            for (var i = 0 ; i < len ; i++) {
                tableStr = tableStr + "<tr><td>" + dataArray[i].ORDER_CODE + "</td>" + "<td>" + dataArray[i].CUSTOMER_NAME + "</td>" + "<td>" + dataArray[i].CUSTOMER_NAME + "</td>" + "<td>" + dataArray[i].COM_NAME + "</td>" + "<td>" + dataArray[i].ORDER_COUNT + "</td>" + "<td>" + date + "</td>" + "<td>" + dataArray[i].PRODUCE_STATUS_NAME + "</td></tr>";
            }
            tableStr = tableStr + "</table>";
            $("#produceinfo").html(tableStr);
        }
    });
}