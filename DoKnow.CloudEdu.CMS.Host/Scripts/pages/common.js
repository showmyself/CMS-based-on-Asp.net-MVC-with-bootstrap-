﻿var MB = window["MB"] || {}
MB.page = MB["page"] || {}
MB.page.common = (function ($) {
    // 对Date的扩展，将 Date 转化为指定格式的String 
    // 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
    // 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
    // 例子： 
    // (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
    // (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 

    var openwindow = function(url, name, iWidth, iHeight) {
        var url; //转向网页的地址;
        var name; //网页名称，可为空;
        var iWidth; //弹出窗口的宽度;
        var iHeight; //弹出窗口的高度;
        var iTop = (window.screen.availHeight - 30 - iHeight) / 2; //获得窗口的垂直位置;
        var iLeft = (window.screen.availWidth - 10 - iWidth) / 2; //获得窗口的水平位置;
        window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=auto,resizeable=no,location=no,status=no');
    }
    var getQueryParam = function(name) {
        var result = location.search.match(new RegExp("[\?\&]" + name + "=([^\&]+)", "i"));
        if (result == null || result.length < 1) {
            return "";
        }
        return result[1];
    }
    var setBackData = function () {
        var urlparts = window.location.href.split('?')
        if (urlparts.length > 0)
        {
            for (var i = 0; i < urlparts.length; i++) {
                urlparts[i]
            }
        }
            var detailUrl = $('a[data-type="detail"]').attr('href')
        $('a[data-type="detail"]').attr('href', detailUrl)
    }
    var getBackData = function () {

    }
    var addArrayData = function (colloIdArr, data) {
        colloIdArr.push(data)
    }
    var removeArrayData = function (colloIdArr, data) {
        for (var i = 0; i < colloIdArr.length; i++) {
            if (colloIdArr[i] == data) {
                colloIdArr.splice(i, 1);
                return;
            }
        }
    }
    var pageSize = $('input[data-name="page_size"][type="hidden"]').val()
    console.log(pageSize)
    return {
        "openwindow": openwindow,
        "setBackData": setBackData,
        "getBackData": getBackData,
        "getQueryParam": getQueryParam,
        "addArrayData": addArrayData,
        "removeArrayData": removeArrayData,
        "pageSize": pageSize
    }
})(jQuery)

$(document).ready(function () {

})

Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1,                 //月份 
        "d+": this.getDate(),                    //日 
        "h+": this.getHours(),                   //小时 
        "m+": this.getMinutes(),                 //分 
        "s+": this.getSeconds(),                 //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds()             //毫秒 
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

Array.prototype.removeElement = function (data) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == data) {
            this.splice(i, 1);
            return;
        }
    }
}

String.prototype.GetDate = function () {
    if (this == '/Date(631123200000)/') return ""
    var dateTime = new Date(this.substr(6, this.length - 8) - 0)
    return dateTime.Format('yyyy-MM-dd hh:mm:ss')
}

Function.prototype.curry = function () {
    var slice = Array.prototype.slice,
        args = slice.apply(arguments),
        that = this;
    return function () {
        return that.apply(null, args.concat(slice.apply(arguments)));
    };
}