﻿
$(document).ready(function () {
    // 弹出错误信息
    var result = $("#result-msg").val();
    if (typeof result != "undefined" && result.length > 0) {
        alert(result);
        if ($("#add-type").val() == "add") {
            $("#roleCode").val("");
            $("#name").val("");
            $("#remark").val("");
        }
    }

    // 添加
    $("#btnAdd").click(function () {
        location.href = $("#hidAddRole").val();
    });

    // 返回
    $("#btn-return").click(function () {
        location.href = $("#hidReturn").val();
    });

    // 删除
    $(".del-role").click(function () {
        if (!confirm("是否删除该角色？")) {
            return;
        }
        var id = $(this).data("id");
        var thisHtml = $(this).parents(".row-content");
        $.post("DeleteRoleByID", { id: id }).done(function (data) {
            thisHtml.hide();
            alert(data);
        }).fail(function () { alert("删除失败"); });
    });
});