﻿
//审核通过
$("a[data-type='btnAccept']").click(function () {
    if ($(this).attr('disabled') === 'disabled') return;
    var shipId = $('#btnAccept').attr('data-id');
    var requestData = {
        ID: shipId,
        STATUS: 2
    }
    $.post('/Ship/AcceptShip',
        {
            shipInfo: JSON.stringify(requestData)

        }, function (data) {
            if (data == "OK") {
                $('a[data-type="btnAccept"]').attr('disabled', 'disabled')
                $('a[data-type="btnCancel"]').attr('disabled', 'disabled')
            }
        }, 'json'
        )
});


//驳回
$("a[data-type='btnCancel']").click(function () {
    if ($(this).attr('disabled') === 'disabled') return;
    if (confirm("确定要驳回发货单吗？")) {
        $('#myRefuse').modal('show');
    }
    else {
        $('#myRefuse').modal('hide');

    }
});

$('#btnConfirmRefuse').click(function () {
    var shipId = $('#btnCancel').attr('data-id')
    var requestData = {
        ID: shipId,
        STATUS: 9,
        REASON: $('#texRefuse').val()
    }
    $.post("/Ship/CancelShip"
        , { shipInfo: JSON.stringify(requestData) }
        , function (data) {
            if (data == "OK") {
                $('a[data-type="btnAccept"]').attr('disabled', 'disabled')
                $('a[data-type="btnCancel"]').attr('disabled', 'disabled')
                $('#myRefuse').modal('hide');
            }
        }, "json")
});


