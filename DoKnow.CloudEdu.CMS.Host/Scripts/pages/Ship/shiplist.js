﻿var MB = window["MB"] || {}
MB.page = MB["page"] || {}
MB.page.shiplist = (function ($) {
    var settings = {
        shipData: [],
        position: 0,
        showModal:0,
        currentShipData:null
    }
    var setModalData = function () {
        if (settings.shipData.length == 0) {
            alert("没有发货单了")
            return
        }
        if (settings.position + 1 > settings.shipData.length) {
            alert('本页已录完,进入下一页')
            reload();return
        }
        $('#modal-expressno').val('')
        settings.currentShipData = $.parseJSON(settings.shipData[settings.position])
        $('#modal-position').text("当前项 " + (settings.position + 1) + "/" + settings.shipData.length)
        $('#modal-receiver').val(settings.currentShipData.RECIEVER)
        $('#modal-mobile').val(settings.currentShipData.RECIEVER_MOBILE)
        $('#modal-address').val(settings.currentShipData.RECIEVER_ADDRESS)
        $('#modal-remark').val(settings.currentShipData.REMARK)
        $('#modal-orderCode').val(settings.currentShipData.ORDER_CODE)
        settings.position = settings.position + 1 
    }
    var initModal = function () {
        settings.shipData = []
        settings.position = 0
        $('tr[data-type="data"][has-delivery="0"]').each(function () {
            settings.shipData.push($(this).attr('data-bind'))
        })
        setModalData();
        $('#myDelivery').modal('show')
        $('#modal-expressno').val('')
        setTimeout("$('#modal-expressno').focus()", 700)
    }
    var reload = function () {
        var param = window.location.href.split('?')
        if (param.length < 2) {
            window.location.href = window.location.href + "?showModal=" + settings.showModal
        }
        else
        {
            if (window.location.href.indexOf("showModal") > 0) {
                window.location.href.replace("showModal=1", "showModal=" + settings.showModal)
                window.location.href.replace("showModal=0", "showModal=" + settings.showModal)
                window.location.href = window.location.href
            } else {
                window.location.href = window.location.href + "&showModal=" + settings.showModal
            }
        }
    }
    var saveDeliveryInfo = function () {
        console.log(settings.currentShipData)
        $.post('/Ship/AddDelivery',
            {
                "shipDataText": JSON.stringify(settings.currentShipData),
                "expressNo": $('#modal-expressno').val()
            },
            function (data) {
                if (data == "OK") {
                    $('#row_' + settings.currentShipData.ID).attr('has-delivery', 1)
                    $('#row_' + settings.currentShipData.ID).addClass('active')
                    $('#delivery_' + settings.currentShipData.ID).html($('#modal-expressno').val())
                    setModalData()
                } else {
                    alert('本条保存不成功')
                    setModalData()
                }
            },
            'json'
            )
    }
    return {
        'settings': settings,
        'setModalData': setModalData,
        'initModal': initModal,
        'reload': reload,
        'saveDeliveryInfo': saveDeliveryInfo
    }
})(jQuery)
$(document).ready(function () {
    MB.page.shiplist.settings.showModal = $('#hidden_ShowModal').val()
    MB.page.shiplist.settings.shipData = []
    MB.page.shiplist.settings.position = 0

    $('#btnDelivery').click(function () {
        MB.page.shiplist.initModal()
        MB.page.shiplist.settings.showModal = 1
    })
    $('#btnConfirmSchedule').click(function () {
        MB.page.shiplist.saveDeliveryInfo()
    })
    $("a[data-type='print']").click(function () {
        var common = MB.page.common
        common.openwindow('/Delivery/_DeliveryPrint?modelInfo={"FACTORY_ID":1,"ORDER_CODE":"201509220000006621","PRE_CODE":"F201510550000000011","LOCAL_COM_ID":24,"CTM_COM_ID":0,"COM_CODE":"String","COM_NAME":"String","ORDER_COUNT":2,"BRAND_CODE":"MB","BRAND_NAME":"美特斯邦威","TAG_PRICE":0.00,"SALE_PRICE":0.00,"CUSTOM_FEE":0.00,"FABRIC_CODE":"08夏校梭2603SP","FABRIC_NAME":"08夏校梭2603SP","COLOR_CODE":"40","COLOR_NAME":"土耳其蓝","EXPRESS_ID":"","EXPRESS_NO":"2343245","EXPRESS_TIME":"1990-01-01","RECIEVER_NAME":"李四","PAY_TYPE":0,"FEE":0.0,"SENDER_NAME":"","SEND_TIME":"1990-01-01","EXPRESS_DES":"","ZIP":"","ADDRESS":"纬地路88弄80号401室","MOBILE":"18868878840","PHONE":"","REMARK":"不包","SHIP_TIME":"2015-11-10","SHIP_BEGIN_TIME":"1990-01-01","SHIP_END_TIME":"1990-01-01","SHIP_STATUS":0,"SHIP_STATUS_TEXT":"UNKNOWN","DELIVERY_STATUS":1,"DELIVERY_STATUS_TEXT":"已发货","PageIndex":1,"PageSize":20,"IsDelete":false}'
            , "快递单预览"
            , 1400, 600)

    })
    if (MB.page.shiplist.settings.showModal == 1) {
        setTimeout("$('#btnDelivery').trigger('click')",500)
    }
    window.onkeydown = function (e) {
        if (e.keyIdentifier === "F9") {
            $('#btnConfirmSchedule').trigger('click')
        }
        if (e.keyIdentifier === "F8") {
            $('#btnDelivery').trigger('click')
        }
    }

})