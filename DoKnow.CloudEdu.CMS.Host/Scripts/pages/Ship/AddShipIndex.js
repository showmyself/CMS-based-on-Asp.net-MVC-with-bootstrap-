﻿$(document).ready(function () {
    if ($("#receiverInfo").val() == "" || $("#comDetailList").val() == "") {
        $("#btnSave").attr("disabled", true);
    }
});

function AddShipIndex() {
    var ids = GetSelectData();
    if (ids == "") {
        alert("请选择待出货信息");
    }
    else {
        var idsArray = new Array();
        idsArray = ids.toString().split(",");

        $.ajax({
            type: "POST",
            url: "/Ship/GetShipInfo?IDS=" + JSON.stringify(idsArray), //将后台操作放在此文件里
            success: function (data) {
                //alert(data);
                var dataArray = eval(data.comDetailList);
                if (data.receiverInfo == null) {
                    $("#shipInfo").modal('hide');
                    return;
                } else {
                    $("#modal-receiver").val(data.receiverInfo.RECEIVE_NAME);
                    $("#modal-mobile").val(data.receiverInfo.RECEIVE_MOBILE);
                    $("#modal-address").val(data.receiverInfo.RECEIVE_ADDRESS);
                }

                if (data.comDetailList == null) {
                    $("#shipInfo").modal('hide');
                    return;
                }
                else {

                    var tableStr = "<table class=\"table table-striped table-hover table-bordered\" >";
                    tableStr = tableStr + "<thead><td>订单号</td><td>客户名</td><td>商品编码</td><td>商品名称</td><td>定制数量</td></thead>";
                    var len = dataArray.length;

                    for (var i = 0 ; i < len ; i++) {
                        tableStr = tableStr + "<tr><td>" + dataArray[i].ORDER_CODE + "</td>" + "<td>" + dataArray[i].CUSTOMER_NAME + "</td>" + "<td>" + dataArray[i].COM_CODE + "</td>" + "<td>" + dataArray[i].COM_NAME + "</td>" + "<td>" + dataArray[i].ORDER_COUNT + "</td></tr>";
                    }
                    tableStr = tableStr + "</table>";

                    $("#divCom").html(tableStr);

                    $('#shipInfo').modal('show');
                }

            }
        });

    }
}

//新增出货单
function SaveShipInfo() {
    if ($("#SHIP_TIME").val() == "") {
        alert("出货日期不能为空!");
        $("#SHIP_TIME").focus();
        return;
    }
    var jsondata = GetData();

    if (confirm("确认保存吗?")) {

        $.post('/ShipAdd/SaveShip', { shipInfo: jsondata }, function (data) {
            alert(data);
            if (data == "保存成功") {
                $("#divCom").html("");
                $("#shipInfo").modal('hide');
                $("form[name='AddShipIndexForm']").submit();
            }
            else {
                alert("保存失败,原因：" + data);
            }
           
        },
            "json"
            )
    }
}

function GetSelectData() {
    var id = document.getElementsByName('chkId');
    var value = new Array();
    for (var i = 0; i < id.length; i++) {
        if (id[i].checked)
            value.push(id[i].value);
    }

    return value;
}


//获取数据
function GetData() {

    var ids = GetSelectData();

    var idsArray = new Array();
    idsArray = ids.toString().split(",");
    for (var j = 0, max = idsArray.length; j < max; j++) {
        idsArray[j] = {
            "PRE_PRODUCE_ID": parseInt(idsArray[j])
        }
    }

    var AddData = "{";
    //出货单
    AddData += '"SHIP_TIME":"' + $("#SHIP_TIME").val() + '",';

    //备注
    AddData += '"REMARK":"' + $("#txtRemark").val() + '",';


    //预生产单ID
    AddData += '"detailList":' + JSON.stringify(idsArray) + '';

    AddData += "}";

    return AddData;
};

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}
