﻿
//下发生产
$("a[data-type='btnSendProduce']").click(function () {
    if ($(this).attr('disabled') === 'disabled') return;
    var ProduceId = $('#btnSendProduce').attr('data-id');
    var idsArray = new Array();
    idsArray = ProduceId.split(",");
    $.post('/Produce/SendProduce',
        {
            IDS: JSON.stringify(idsArray)

        }, function (data) {
            $('a[data-type="btnSendProduce"]').attr('disabled', 'disabled')
            alert(data);
        }, 'json'
        )
});

$("a[data-type='btnconfirmComplete']").click(function () {
    if ($(this).attr('disabled') === 'disabled') return;
    var ID = $('#btnconfirmComplete').attr('data-id');
    var idsArray = new Array();
    idsArray = ID.split(",");

    $.post('/Produce/CompleteProduce',
        {
            IDS: JSON.stringify(idsArray)

        }, function (data) {
            $('a[data-type="btnconfirmComplete"]').attr('disabled', 'disabled')
            alert(data);
        }, 'json'
        )
});

window.onload = function () {

    var progresstable = document.getElementById("progresstable");
    //如果在表格区域内产生单击 
    progresstable.onclick = function (e) {
        var e = window.event || e, target = e.srcElement || e.target;

        //得到tr 
        while (target.tagName.toLowerCase() != "tr") {
            target = target.parentNode;
        }

        var i = target.rowIndex;
        //单击单行的Tr的话就是控制tr + 1的隐藏和显示 

        if (i % 2 == 0) {
            var nrs = progresstable.rows[i + 1].parentNode.style;
            nrs.display = nrs.display == "none" ? "" : "none";
        }
    }

    var versiontable = document.getElementById("versiontable");
    //如果在表格区域内产生单击 
    versiontable.onclick = function (e) {
        var e = window.event || e, target = e.srcElement || e.target;

        //得到tr 
        while (target.tagName.toLowerCase() != "tr") {
            target = target.parentNode;
        }

        var i = target.rowIndex;
        //单击单行的Tr的话就是控制tr + 1的隐藏和显示 

        if (i % 2 == 0) {
            var nrs = versiontable.rows[i + 1].parentNode.style;
            nrs.display = nrs.display == "none" ? "" : "none";
        }
    }

    var clothestable = document.getElementById("clothestable");
    //如果在表格区域内产生单击 
    clothestable.onclick = function (e) {
        var e = window.event || e, target = e.srcElement || e.target;

        //得到tr 
        while (target.tagName.toLowerCase() != "tr") {
            target = target.parentNode;
        }

        var i = target.rowIndex;
        //单击单行的Tr的话就是控制tr + 1的隐藏和显示 

        if (i % 2 == 0) {
            var nrs = clothestable.rows[i + 1].parentNode.style;
            nrs.display = nrs.display == "none" ? "" : "none";
        }
    }

    var bodytable = document.getElementById("bodytable");
    //如果在表格区域内产生单击 
    bodytable.onclick = function (e) {
        var e = window.event || e, target = e.srcElement || e.target;

        //得到tr 
        while (target.tagName.toLowerCase() != "tr") {
            target = target.parentNode;
        }

        var i = target.rowIndex;
        //单击单行的Tr的话就是控制tr + 1的隐藏和显示 

        if (i % 2 == 0) {
            var nrs = bodytable.rows[i + 1].parentNode.style;
            nrs.display = nrs.display == "none" ? "" : "none";
        }
    }
}

function tableClick(tableid)
{
    var table = document.getElementById("" + tableid + "");
    
    //如果在表格区域内产生单击 
    table.onclick = function (e) {
        var e = window.event || e, target = e.srcElement || e.target;

        //得到tr 
        while (target.tagName.toLowerCase() != "tr") {
            target = target.parentNode;
        }

        var i = target.rowIndex;
        //单击单行的Tr的话就是控制tr + 1的隐藏和显示 

        if (i % 2 == 0) {
            var nrs = table.rows[i + 1].parentNode.style;
            nrs.display = nrs.display == "none" ? "" : "none";
        }
    }
}