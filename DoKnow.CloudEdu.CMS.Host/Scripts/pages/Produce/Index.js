﻿
function GetDateDiff(startDate, endDate) {
    var startTime = new Date(Date.parse(startDate.replace(/-/g, "/"))).getTime();
    var endTime = new Date(Date.parse(endDate.replace(/-/g, "/"))).getTime();
    var dates = Math.abs((startTime - endTime)) / (1000 * 60 * 60 * 24);
    return dates;
}

function GetQueryData() {
    var QueryProduce = {};
    QueryProduce["ORDER_CODE"] = $("#ORDER_CODE").val();//订单号
    QueryProduce["CODE"] = $("#CODE").val();//生产单号
    QueryProduce["STATUS"] = $("#status").val() == "" ? "0" : $("#STATUS").val();//生产单状态
    QueryProduce["ORDER_BEGIN_TIME"] = $("#ORDER_BEGIN_TIME").val();//订单创建开始时间
    QueryProduce["ORDER_END_TIME"] = $("#ORDER_END_TIME").val();//订单创建结束时间

    return QueryProduce;
}

function CliExport() {
    if ($("#ORDER_BEGIN_TIME").val() == "" || $("#ORDER_END_TIME").val() == "") {
        alert("请选择创建订单时间！"); return false;
    }
    else {
        var startDate = $("#ORDER_BEGIN_TIME").val();
        var endDate = $("#ORDER_END_TIME").val();
        var days = GetDateDiff(startDate, endDate);
        if (days > 7) {
            alert("单次查询最大时间跨度为7天！");
            return false;
        }
    }
    var QueryProduce = {};
    QueryProduce["ORDER_CODE"] = $("#ORDER_CODE").val();//订单号
    QueryProduce["CODE"] = $("#CODE").val();//生产单号
    QueryProduce["STATUS"] = $("#status").val() == "" ? "0" : $("#STATUS").val();//生产单状态
    QueryProduce["ORDER_BEGIN_TIME"] = $("#ORDER_BEGIN_TIME").val();//订单创建开始时间
    QueryProduce["ORDER_END_TIME"] = $("#ORDER_END_TIME").val();//订单创建结束时间
    var link = "/Produce/Export?QueryProduce=" + JSON.stringify(QueryProduce);
    window.open(link);
}
function SendProduce() {
    //下发生产
    if (confirm("确定要下发生产吗？")) {
        var ProduceId = $('#btnSendProduce').attr('data-id');
        var idsArray = new Array();
        idsArray = ProduceId.split(",");
        $.post('/Produce/SendProduce',
            {
                IDS: JSON.stringify(idsArray)
            }, function (data) {
                if (data == "下发成功") {
                    $("form[name='ProduceForm']").submit();
                }
            }, 'json'
            )
    }
}
function CompleteProduce() {
    if (confirm("确定要确认完工吗？")) {
        var ID = $('#btnconfirmComplete').attr('data-id');
        var idsArray = new Array();
        idsArray = ID.split(",");

        $.post('/Produce/CompleteProduce',
            {
                IDS: JSON.stringify(idsArray)

            }, function (data) {
                alert(data)
                if (data == "确认完工成功") {
                    $("form[name='ProduceForm']").submit();
                }
            }, 'json'
          );
    }
}

function SelectAll() {
    if ($('#checkedAll').prop('checked')) {
        $("input[name='chkId']").each(function (i, n) {
            $(n).attr('checked', true);
        });
    } else {
        $("input[name='chkId']").each(function (i, n) {
            $(n).removeAttr("checked");
        });
    }
}

function SendAll() {
    var ids = GetSelectData();
    var idsArray = new Array();
    idsArray = ids.toString().split(",");
    if (ids != "") {
        if (confirm("确定要下发生产吗？")) {
            var ProduceId = $('#btnSendProduce').attr('data-id')
            $.post('/Produce/SendProduce',
                {
                    IDS: JSON.stringify(idsArray)
                }, function (data) {
                    if (data == "下发成功") {
                        $("form[name='ProduceForm']").submit();
                    }
                    else {
                        alert("下发失败,原因：" + data);
                    }
                }, 'json'
              );
        }
    }
    else {
        alert("请选择数据!");
    }
}


function CompeleteAll() {
    var ids = GetSelectData();
    var idsArray = new Array();
    idsArray = ids.toString().split(",");
    if (ids != "") {
        if (confirm("确定要确认完工吗？")) {
            var ProduceId = $('#btnconfirmComplete').attr('data-id')
            $.post('/Produce/CompleteProduce',
                {
                    IDS: JSON.stringify(idsArray)
                }, function (data) {
                    if (data == "确认完工成功") {
                        $("form[name='ProduceForm']").submit();
                    }
                    else {
                        alert("确认完工失败,原因：" + data);
                    }
                }, 'json'
              );
        }
    }
    else {
        alert("请选择数据!");
    }
}

function GetSelectData() {
    var id = document.getElementsByName('chkId');
    var value = new Array();
    for (var i = 0; i < id.length; i++) {
        if (id[i].checked)
            value.push(id[i].value);
    }

    return value;
}