﻿
$(document).ready(function () {

    // 弹出错误信息
    var result = $("#result-msg").val();
    if (typeof result != "undefined" && result.length > 0) {
        alert(result);
    }

    // 返回
    $("#btn-add-return").click(function () {
        location.href = $("#hidAddRoleMenu").val();
    });

    // 删除
    $(".del-menu").click(function () {
        if (!confirm("是否删除该角色？")) {
            return;
        }
        var menuId = $(this).data("menuid");
        var roleId = $("#hidRoleId").val();
        var thisHtml = $(this).parents(".menu-content");
        $.ajax({
            url: $("#hidDelUrl").val(),
            type: 'POST',
            data: { menuID: menuId, roleID: roleId }
        }).done(function (data) {
            alert(data);
            window.location.href = $("#hidLocationUrl").val();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert("删除失败");
        });

    });

    // 
    $("#btnGetMenuBack").click(function () {
        location.href = $("#hidGetMenuBackUrl").val();
    });
    
});
